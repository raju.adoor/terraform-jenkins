variable "subscription_id" {
  default = "cbab7e96-4c63-47a6-862a-f7cba65acd4d" 
}
variable "tenant_id" {
  default = "da1d58c9-1e97-40e6-a112-d7786d7c4e0b" 
}

variable "resource_group_name" {
  default = "radar-rg"
}

variable "resource_group_location" {
  default = "eastus"
}

variable "cosmos_db_account_name" {
  default = "radarcosmosdbdemo4"
}

variable "failover_location" {
  default = "eastus2"
}