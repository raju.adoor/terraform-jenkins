terraform {
  required_providers {
    azurerm = {
       version = "=3.2.0"
    }
      azuread = {
      source  = "hashicorp/azuread"
      version = "=1.6.0"
    }
  }
  
}

provider "azuread" {
  environment = "public"
  tenant_id = var.tenant_id
}



provider "azurerm" {
  features {}
  subscription_id            = var.subscription_id
  tenant_id                  = var.tenant_id
  skip_provider_registration = true
}
resource "azurerm_resource_group" "rg" {
  name     = "centralus-recover-sandbox-rg"
  location = "centralus"
}
